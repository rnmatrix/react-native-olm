import * as React from 'react';
import { StyleSheet, View, Text } from 'react-native';
// @ts-ignore
import Olm from 'rn-olm';

export default function App() {
  const myFirstTest = async () => {
    // Olm.get_library_version('hello').then(console.log);
    const account = new Olm.Account();
    // const session = new Olm.Session();

    // account.create();
    // account.identity_keys().then(console.log).catch(console.warn);
    account.identity_keys();
    account.sign('hello wow').then(console.log).catch(console.warn);
    account.one_time_keys().then(console.log).catch(console.warn);
    account.mark_keys_as_published();
    account.max_number_of_one_time_keys();
    account.generate_one_time_keys(2);
    account.generate_fallback_key();
    account.fallback_key();
    account.pickle();
    account.unpickle();

    const inboundGroupSession = new Olm.InboundGroupSession();

    inboundGroupSession.create('session_key');
    inboundGroupSession.decrypt('hello world');
    console.log('session id: ', inboundGroupSession.session_id());
    console.log(
      'inbound group session first known index: ',
      inboundGroupSession.first_known_index()
    );
    console.log('exported session: ', inboundGroupSession.export_session(1));

    // Pass an OlmSession
    // account.remove_one_time_keys();

    Olm.PRIVATE_KEY_LENGTH();
  };

  React.useEffect(() => {
    console.log({ Olm });
    myFirstTest();

    // Olm.getTestString('test').then((str: string) =>
    //   console.log('Finished that ', str)
    // );
    Olm.init();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Welcome to the app!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
  text: {
    fontSize: 24,
  },
});
