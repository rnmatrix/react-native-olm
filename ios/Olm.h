#import <React/RCTBridgeModule.h>
#import <OLMKit/OLMKit.h>

@interface Olm : NSObject <RCTBridgeModule>
{
    OLMAccount *account;

}
@property OLMAccount *account;
@property OLMInboundGroupSession *inboundGroupSession;
@property OLMUtility *utility;
@property OLMSession *session;
@property OLMSAS *olmSAS;
@property OLMPkSigning *olmPkSigning;
@property OLMPkEncryption *olmPkEncryption;
@property OLMPkDecryption *olmPkDecryption;
@property OLMOutboundGroupSession *olmOutboundGroupSession;

@end
